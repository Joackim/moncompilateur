%{
// This is our Lexical tokeniser 
// It should be compiled into cpp with :
// flex++ -d -otokeniser.cpp tokeniser.l 
// And then compiled into object with
// g++ -c tokeniser.cpp
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

#include "tokeniser.h"
#include <iostream>

using namespace std;

%}

%option noyywrap
%option c++
%option yylineno


alpha   [A-Za-z]
stringconst (\"[A-Za-z0-9 \\\(\)\<\>\=\!\%\&\|\}\-\:\;\.]*\")
ws      [ \t\n\r]+
digit   [0-9]
number  ((\-)?{digit}+|True|False|(\-)?{digit}+\.{digit}+)

keyword (IF|THEN|ELSIF|ELSE|WHILE|FOR|TO|DO|VAR|START|STOP|BEGIN|END)
rprocedure (DISP|ABS|SIN|COS|SQRT|CHOP|ROUND)
keytype (BOOL|INT|FLOAT)
id	{alpha}({alpha}|{digit})*
addop	(\+|\-|OR)
mulop	(\*|\/|%|AND)
relop	(\<|\>|"=="|\<=|\>=|!=)
unknown [^\"A-Za-z0-9 \n\r\t\(\)\<\>\=\!\%\&\|\}\-\;\.]+

%%

{addop}		return ADDOP;
{mulop}		return MULOP;
{relop}		return RELOP;
{number}	return NUMBER;
{stringconst}	return CSTRING;
{keyword}	return KEYWORD;
{keytype}   return PTYPE;
{rprocedure}      return RCALL;
{id}		return ID;
"["		return LBRACKET;
"]"		return RBRACKET;
","		return COMMA;
";"		return SEMICOLON;
"."		return DOT;
":="		return ASSIGN;
"("		return LPARENT;
")"		return RPARENT;
"!"		return NOT;
<<EOF>>		return FEOF;

{ws}    {/* skip blanks and tabs */};

">>>"   { /* Skip comments between '/>' and '</' */
            int c;
            while((c = yyinput()) != 0)
            {
                if(c == '<')
                {
					if((c = yyinput()) == '<')
					{       
                        if((c = yyinput()) == '<')
                        {       
                            break;
                        }
                        else
                            unput(c);
                    }
                }	
            }
	    };

{unknown}	return UNKNOWN;

%%

