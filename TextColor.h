
// Unix systems
#if defined( __unix__ ) || defined( __unix )

    enum class TextColor
    {
        Default     = 39,
        White       = 97,
        Blue        = 94,
        Green       = 92,
        Red         = 91,
    };

    std::ostream& operator << ( std::ostream& stream, TextColor textcolor )
    {
        stream << "\e[" + std::to_string( (int)textcolor ) + "m";
        return stream;
    }

#endif 
