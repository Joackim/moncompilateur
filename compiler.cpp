//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make"

#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <map>
#include <iterator> 
#include <cstring>
#include "TextColor.h"



using namespace std;

enum OPREL	{EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD	{ADD, SUB, OR, WTFA};
enum OPMUL	{MUL, DIV, MOD, AND ,WTFM};
enum TYPE	{BOOL, INT, FLOAT, VOID, STRING, UNDEFINED};

map<string, TYPE> Variables;

void Error( string );


TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string
	
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id)
{
	return DeclaredVariables.find(id) != DeclaredVariables.end();
}

string getTypeStr( TYPE ); // forward declaration

void Error(string s)
{
	cerr << TextColor::Red << "/!\\ Error line n°" << lexer->lineno() << ", read '" << lexer->YYText() <<"'.\n   -> '"
		 << s << "'\n" << TextColor::Default << endl;
	exit(-1);
}

void readToken(void)
{
    current=(TOKEN)lexer->yylex();
}

const char* tokenText()
{
    return( lexer->YYText());
}
		
TYPE getVarType( string );

TYPE Identifier(void)
{
	cout << "\tpush " << tokenText() << endl;
	TYPE type = getVarType( tokenText() );
	readToken();
	return type;
}

TYPE getVarType( string var )
{
	map<string, TYPE>::iterator itr;	
	for( itr = Variables.begin(); itr != Variables.end(); ++itr )
	{
		if( itr->first == var ) return itr->second;
	}
	Error("Could not find variable variable '" + var + "'");  
	return UNDEFINED; // avoid warning
}

// Get string from TYME enum, Used for error message
string getTypeStr( TYPE type )
{
	switch( type )
	{
		case BOOL :
			return "BOOL";
			break;

		case INT  :
			return "INT";
			break;

		case FLOAT :
			return "FLOAT";
			break;
		
		case VOID :
		   return "VOID";
			break;	   

		case STRING :
			return "STRING";
			break;
	}
	Error("Unknown Type");
	return "ERROR";
}

// Get TYPE enum quivalent from pascal string
TYPE getType( string Type )
{
	if( Type == "BOOL" ) return BOOL;
	else if( Type == "INT" ) return INT;
	else if( Type == "FLOAT" ) return FLOAT;
	else if( Type == "STRING") return STRING;
	else if( Type == "VOID") return VOID;
	else
	{
		Error("Error, Unexpected Type ");
		return UNDEFINED;
	}
}

// get ASM equivalent from TYME enum
string getAsmType( TYPE type )
{
	string asmType = "Failed";
	switch( type )
	{
		case BOOL :
			asmType = ".quad 0";
			break;

		case INT :	
			asmType = ".quad 0";
			break;

		case FLOAT :
			asmType = ".double 0";
			break;
	}	

	if( asmType == "Failed")
	{
		Error("Error, Type unknown");
		return "unkown"; // avoir warning
	}
	return asmType;
}


TYPE Number(void)
{
	TYPE type;	
	string num = tokenText();
	if( num == "True" or num == "False" )  // add possibility to assigne bool with TRUE and FALSE
	{
		if( num == "True" ) num = "0xFFFFFFFFFFFFFFFF";
		else if( num == "False" ) num = "0";
		cout <<"\tpush $" << num << endl;
		type = BOOL;

	}
	else if( num.find('.') != std::string::npos )	// if there is a '.' its a float number 
	{
		double fvalue = stod( num );				// à l'adresse &f, il y a 8 octets qui contiennent le codage en norme IEEE754 du flottant 123.4
		long long signed int *i;					// i est un pointeur sur un espace de 8 octets sensé contenir un entier non signé en binaire naturel

		i= (long long signed int *) &fvalue;		// Maintenant, *i est un entier non signé sur 64 bits qui contient l'interprétation entière du codage IEEE754 du flottant 123.4 
		cout << "\tmovq $" << *i << ", %rax" <<  "\t# empile le flottant " << fvalue << endl;
		cout << "\tpush %rax" << endl;

		type = FLOAT;
	}
	else // regular signed integer
	{
		cout <<"\tpush $" << num << endl;
		type = INT;
	}

	readToken();
	return type; 
}

string escString( const char* cstr ) // retuirn string with escaped \n and \t
{
	string output = "";
	string str = cstr;
    
    for( int i = 0; i < str.length(); i++ )
    {
        char a = str[i];
        if	   ( a == '\n' ){ a = '\\'; cout << 'n'; }
		else if( a == '\t' ){ a = '\\'; cout << 't'; }
        output += a;
    }
	return output;
}

unsigned int str_cpt = 0;
TYPE StringConst(void)
{
	if( current != CSTRING )
		Error("Expected String Expression");
		
	const char* message = tokenText();	
	str_cpt++;
	cout << "\t.data" << endl;
	cout << "msg" << str_cpt << ":" << endl;
	cout << "\t.string  " << escString( message ) << "\t\t\t# the string to print." << endl;
	cout << "\t.text" << endl;

	readToken();

	return STRING;

}



// forward declaration
TYPE Expression(void);			// Called by Term() and calls Term()
TYPE ReservedCall( void );

TYPE Factor(void)
{
	TYPE type;
	if(current == LPARENT)
    {
		readToken();
		type = Expression();
		if(current != RPARENT)
			Error("Expected ')' token");		// ")" expected
		else
			readToken();
	}
	else 
		if (current == NUMBER)
			type = Number();
		else if( current == RCALL)
			type = ReservedCall();
		else if( current == CSTRING )
			type = StringConst();
		else
		{
			if(current == ID) 
				type = Identifier();
			else
				Error("Expected Number, ID or ')' token");
		}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "AND"
OPMUL MultiplicativeOperator(void)
{
	OPMUL opmul;
	if(strcmp(tokenText(),"*") == 0)
		opmul = MUL;
	else if(strcmp(tokenText(),"/") == 0)
		opmul = DIV;
	else if(strcmp(tokenText(),"%") == 0)
		opmul = MOD;
	else if(strcmp(tokenText(),"AND") == 0)
		opmul = AND;
	else opmul = WTFM; // what the fuck man ?!
	readToken();
	return opmul;
}

// TODO factorize code
// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void)
{
	TYPE type1;
	TYPE type2;
	OPMUL mulop;
	type1 = Factor();
	while(current == MULOP)
    {
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2 = Factor();
		cout << "\tpop %rbx" << endl;	// get first operand
		cout << "\tpop %rax" << endl;	// get second operand
		switch(mulop)
        {
			case AND:
				if( type1 == BOOL and type2 == BOOL )
				{
					cout << "\timulq	%rbx" << endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND" << endl;	// store result
					break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for 'AND' operator");
			case MUL:
				if( type1 == INT and type2 == INT )
				{
					cout << "\timulq	%rbx" << endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL" << endl;	// store result

					type1 = INT; break;
					break;
				}
				else if( type1 == FLOAT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfmulp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else if( type1 == INT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfmulp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else if( type1 == FLOAT and type2 == INT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfmulp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '*' operator");
			case DIV:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfdivp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else if( type1 == FLOAT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfdivp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else if( type1 == INT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfdivp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else if( type1 == FLOAT and type2 == INT )
				{
					cout << "\tpush %rbx" << endl;			// second operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfldl (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfdivp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;	

					type1 = FLOAT; break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '/' operator");
			case MOD:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tmovq $0, %rdx" << endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx" << endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD" << endl;		// store result
					break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '\%' operator");
					
			default:
				Error("Multiplicative operator expected");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "OR"
OPADD AdditiveOperator(void)
{
	OPADD opadd;
	if(strcmp(tokenText(),"+") == 0)
		opadd = ADD;
	else if(strcmp(tokenText(),"-") == 0)
		opadd = SUB;
	else if(strcmp(tokenText(),"OR") == 0)
		opadd = OR;
	else opadd = WTFA;
	readToken();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void)
{
	TYPE type1; // first operand
	TYPE type2; // second operand
	OPADD adop;
	type1 = Term();
	while(current == ADDOP)
    {
		adop  = AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		cout << "\tpop %rax" << endl;	// get second operand
		cout << "\tpop %rbx" << endl;	// get first operand
		switch(adop)
        {
			case OR: // Boolean operation 
				if( type1 == BOOL and type2 == BOOL )
				{
					cout << "\taddq	%rbx, %rax\t# OR" << endl; // operand1 OR operand2
					cout << "\tpush %rax" << endl;			// store result
					type1 = BOOL;
				}
				else	
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for 'OR' operator");

			case ADD:
				if( type1 == INT and type2 == INT )
				{
					cout << "\taddq	%rbx, %rax\t# ADD" << endl;	// add both operands
					cout << "\tpush %rax" << endl;			// store result

					type1 = INT; break;
				}
				else if( type1 == FLOAT and type2 == INT )
				{
					cout << "\tpush %rax" << endl;			// second operand
					cout << "\tfild (%rsp)" << endl;		// load as Float in FPU
					cout << "\taddq $8, %rsp" << endl;		// clear one value from rsp
					cout << "\tpush %rbx" << endl;			// first operand
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
					cout << "\tfaddp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else if( type1 == INT and type2 == FLOAT )
				{
					cout << "\tpush %rax" << endl;			// second operand
					cout << "\tpush %rbx" << endl;			// first operand
					cout << "\tfild (%rsp)" << endl;	
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
					cout << "\tfaddp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else if( type1 == FLOAT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;
					cout << "\tpush %rax" << endl;
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	// put expr	st[ op2 ]
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	// put expr	st[ op1; op2 ]
					cout << "\tfaddp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '+' operator");
			case SUB:	
				if( type1 == INT and type2 == INT )
				{
					cout << "\tsubq	%rax, %rbx\t# SUB" << endl;	// substract both operands
					cout << "\tpush %rbx" << endl;			// store result

					type1 = INT; break;
				}
				else if( type1 == FLOAT and type2 == INT )
				{
					cout << "\tpush %rax" << endl;			// second operand
					cout << "\tfild (%rsp)" << endl;		// load as Float in FPU
					cout << "\taddq $8, %rsp" << endl;		// clear one value from rsp
					cout << "\tpush %rbx" << endl;			// first operand
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
					cout << "\tfsubp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else if( type1 == INT and type2 == FLOAT )
				{

					cout << "\tpush %rax" << endl;			// first operand
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
					cout << "\tpush %rbx" << endl;			// firt operand
					cout << "\tfild (%rsp)" << endl;		// load as Float in FPU
					cout << "\taddq $8, %rsp" << endl;		// clear one value from rsp
					cout << "\tfsubp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}
				else if( type1 == FLOAT and type2 == FLOAT )
				{
					cout << "\tpush %rbx" << endl;
					cout << "\tpush %rax" << endl;
					cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	// put expr	st[ op2 ]
					cout << "\taddq $8, %rsp" << endl;
					cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	// put expr	st[ op1; op2 ]
					cout << "\tfsubp" << endl;									// st[ op1 + op2 ] 
					cout << "\tfstpl (%rsp)     # push addition value and pop st" << endl;			// overwrite top of rsp with expr

					type1 = FLOAT; break;
				}

				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '-' operator");
			default:
				Error("Unknown additive operator");
		} // end of swtich

	}				
	return type1; // no operator
}

void SemiColon(void);

// DeclarationSubPart := PTYPE ":" ID { "," ID } ";"
void DeclarationSubPart(void)
{
	TYPE Type = getType( tokenText() );
	string asmType = getAsmType( Type );

	readToken();

	if( strcmp( tokenText() ,":") != 0) Error("Expected ':' token");
	readToken();	// read ':' token


	if(current != ID)
		Error("Error : identifier expected");

	string var = tokenText();								// get var id
	cout << var << ":\t\t" + asmType << endl;				// generate asembly
	DeclaredVariables.insert( var );						// put var in declared set
	Variables.insert( pair<string, TYPE>( var, Type ));		// put var and its type in Variables map
	readToken();											// read var

	while(current == COMMA)
    {
		readToken();											// read comma
		if(current != ID)
			Error("Error : identifier expected");

		string var = tokenText();								// get var id
		cout << var << ":\t\t" + asmType << endl;				// generate asembly
		DeclaredVariables.insert( var );						// put var in declared set
		Variables.insert( pair<string, TYPE>( var, Type ));		// put var and its type in Variables map
		readToken();											// read var
	}
	SemiColon();

}

// DeclarationPart := "VAR" TYPE : ID {"," ID} { ";" TYPE : ID {"," ID}} "."
void DeclarationPart(void)
{

	if( strcmp( tokenText() ,"VAR") == 0)
	{
		readToken();
	}
	else
		Error("Keyword 'VAR' Expecter");

	cout << "\t.data" << endl;
    cout << "FormatStrInt:    \t\t.string \"%lld\"    # used by printf to display 64-bit signed integers" << endl;
	cout << "FormatStrBoolFalse:   \t.string \"FALSE\"    # used by printf to display False Boolean" << endl;
	cout << "FormatStrBoolTrue:   \t.string \"TRUE\"    # used by printf to display True Boolean" << endl;
	cout << "FormatStrChar:   \t\t.string \"%c\"    # used by printf to display a Char" << endl;
	cout << "FormatStrFloat:   \t\t.string \"%F\"    # used by printf to display a Float" << endl;


	cout << "\t.align 8"<< endl;

	while( current == PTYPE )
	{
		DeclarationSubPart();
	}


}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(tokenText(),"==") == 0)
		oprel = EQU;
	else if(strcmp(tokenText(),"!=") == 0)
		oprel = DIFF;
	else if(strcmp(tokenText(),"<") == 0)
		oprel = INF;
	else if(strcmp(tokenText(),">") == 0)
		oprel = SUP;
	else if(strcmp(tokenText(),"<=") == 0)
		oprel = INFE;
	else if(strcmp(tokenText(),">=") == 0)
		oprel = SUPE;
	else oprel = WTFR;
	readToken();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void)
{
	unsigned int tagNbr = ++TagNumber;
	OPREL oprel;
	TYPE type1;
	TYPE type2;
	type1 = SimpleExpression();
	if(current == RELOP)
    {
		oprel = RelationalOperator();
		type2 = SimpleExpression();
		cout << "\tpop %rax" << endl;
		cout << "\tpop %rbx" << endl;
		cout << "\tcmpq %rax, %rbx" << endl;
		switch(oprel)
        {
			// comparison needs same type
			case EQU:
				if( type1 == type2 )
				{
					cout << "\tje Vrai" << tagNbr << "\t# If equal" << endl;
					break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '==' operator");
			case DIFF:
				if( type1 == type2 )
				{
					cout << "\tjne Vrai" << tagNbr << "\t# If different" << endl;
					break;
				}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '!=' operator");			// do not allow boolean
			case SUPE:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tjge Vrai" << tagNbr << "\t# If above or equal" <<endl;
					break;
				}
				else
				{
					if(( type1 == INT or type1 == FLOAT ) and ( type2 == INT or type2 == FLOAT ))
					{

						if( type1 == INT ) 
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}	
						else if( type1 == FLOAT )
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	
						}
						if( type2 == INT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}
						else if( type2 == FLOAT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
						}
						cout << "\taddq $16, %rsp" << endl;
						cout << "\tfcomip %st(1)" << endl;									//	st[ -1.0; expr ] 
						cout << "\tjc Vrai" << tagNbr << endl;			// overwrite top of rsp with expr
						cout << "\tjz Vrai" << tagNbr << endl;
						break;
					}
				else
					Error("Incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '>=' operator");	
				}
			case INFE:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tjle Vrai" << tagNbr << "\t# If below or equal" << endl;
					break;
				}
				else
				{
					if(( type1 == INT or type1 == FLOAT ) and ( type2 == INT or type2 == FLOAT ))
					{
						if( type2 == INT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}
						else if( type2 == FLOAT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
						}
						if( type1 == INT ) 
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}	
						else if( type1 == FLOAT )
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	
						}

						cout << "\taddq $16, %rsp" << endl;
						cout << "\tfcomip %st(1)" << endl;									//	st[ -1.0; expr ] 
						cout << "\tjc Vrai" << tagNbr << endl;			// overwrite top of rsp with expr
						cout << "\tjz Vrai" << tagNbr << endl;
						break;
					}
					else
						Error("incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '<=' operator");	
				}


			case INF:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tjl Vrai" << tagNbr << "\t# If below" << endl;
					break;
				}
				else
				{
					if(( type1 == INT or type1 == FLOAT ) and ( type2 == INT or type2 == FLOAT ))
					{
						if( type2 == INT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}
						else if( type2 == FLOAT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
						}
						if( type1 == INT ) 
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}	
						else if( type1 == FLOAT )
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	
						}

						cout << "\taddq $16, %rsp" << endl;
						cout << "\tfcomip %st(1)" << endl;									//	st[ -1.0; expr ] 
						cout << "\tjc Vrai" << tagNbr << endl;			// overwrite top of rsp with expr
						break;
					}
					else
						Error("incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '<' operator");	
				}

			case SUP:
				if( type1 == INT and type2 == INT )
				{
					cout << "\tjg Vrai" << tagNbr << "\t# If above" << endl;
					break;
				}
				else
				{
					if(( type1 == INT or type1 == FLOAT ) and ( type2 == INT or type2 == FLOAT ))
					{
						if( type1 == INT ) 
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}	
						else if( type1 == FLOAT )
						{
							cout << "\tpush %rbx" << endl;			// first operand
							cout << "\tfldl (%rsp)     # put operand1 to st( 0 )" << endl;	
						}
						if( type2 == INT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfild (%rsp)" << endl;		// convert as float
						}
						else if( type2 == FLOAT )
						{
							cout << "\tpush %rax" << endl;			// second operand
							cout << "\tfldl (%rsp)     # put operand2 to st( 0 )" << endl;	
						}

						cout << "\taddq $16, %rsp" << endl;
						cout << "\tfcomip %st(1)" << endl;									//	st[ -1.0; expr ] 
						cout << "\tjc Vrai" << tagNbr << endl;			// overwrite top of rsp with expr
						break;
					}
					Error("incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for '>' operator");	
				}

			default:
				Error("Unknown comparison operator");
		}
		cout << "\tpush $0\t\t# False" << endl;
		cout << "\tjmp Suite" << tagNbr << endl;
		cout << "Vrai" << tagNbr << ":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True" << endl;	
		cout << "Suite" << tagNbr << ":" << endl;
		return( BOOL );
		
	}
	return type1; // no operator, return type of lone operande

}

// AssignementStatement := Identifier ":=" SimpleExpression
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type
string AssignementStatement(void)
{
	TYPE type1;					// type of id
	TYPE type2;					// type of expr
	string var;
	if(current != ID)
		Error("Expected identifier");
	if(!IsDeclared(tokenText()))
    {
		cerr << "Error : Variable '" << tokenText() << "' not declared" << endl;
		exit(-1);
	}
	var=tokenText();
	readToken(); // read token
	if(current != ASSIGN)
		Error("token ':=' expecter");
	readToken();
	type1 = getVarType( var );
	type2 = Expression();
	if( type1 == type2 )
	{
		cout << "\tpop " << var << endl;
	}
    else if( type1 == FLOAT and type2 == INT ) // allow int assignment to float
    {
        cout << "\tfild (%rsp)" << endl;
        cout << "\tfstpl (%rsp)" << endl;
        cout << "\tpop " << var << endl;
    }
	else
		Error("incompatible types, (" + getTypeStr( type1 ) + ", " + getTypeStr( type2 ) + ") for assignement operator ':='");

	return( var );
}

// Forward declaration
void Statement(void);

void IfSubStatementIF( int nbr )
{
	unsigned int tagNbr = nbr;
	cout << "IF" << tagNbr << ":" <<  endl;
	readToken();									// read 'IF' token

	TYPE type = Expression();						// read Expression()
	if(	type != BOOL )								// check return type
		Error("Expected BOOL Expression, not " + getTypeStr( type ));

	cout << "\tpop %rax" << endl;					// put Expression in %rax
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tjz END_SUB_IF" << tagNbr << endl;	// expr is False go at the end, do not execute THEN
	if( strcmp(tokenText(), "THEN") == 0)
	{
		cout << "# THEN IF " << endl;
		readToken();								// read 'THEN' token
		Statement();								// Generate assembly for Statement()
		cout << "\tjmp ENDIF" << tagNbr << endl;	// expr was True, THEN executed, break out of If block
		cout << "END_SUB_IF" << tagNbr << ":" << endl;
	}
	else
		Error("Expected THEN keyword");
}

void IfSubStatementELSIF( int nbr, int offset )
{
	unsigned int tagNbr = nbr;
	readToken();									// read 'ELSIF' token
	TYPE type = Expression();						// read Expression()
	if(	type != BOOL )								// check return type
		Error("Expected BOOL Expression, not " + getTypeStr( type ));

	cout << "\tpop %rax" << endl;					// put Expression in %rax
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tjz END_SUB_ELSIF" << tagNbr << "_" << tagNbr + offset << endl;	// expr is False go at the end, do not execute THEN
	if( strcmp(tokenText(), "THEN") == 0)
	{
		cout << "# THEN ELSIF " << offset << endl;
		readToken();								// read 'THEN' token
		Statement();								// Generate assembly for Statement()
		cout << "\tjmp ENDIF" << tagNbr << endl;	// expr was True, THEN executed, break out of If block
		cout << "END_SUB_ELSIF" << tagNbr << "_" << tagNbr + offset << ":" << endl;
	}
	else
		Error("Expected THEN keyword");
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void)
{
	unsigned int tagNbr = ++TagNumber;
    if( strcmp(tokenText(), "IF") == 0)
    {
		IfSubStatementIF( tagNbr );
	}
	int offset = 1;
	while( strcmp(tokenText(), "ELSIF") == 0)
	{
		IfSubStatementELSIF( tagNbr, offset++ );
	}
	if( strcmp(tokenText(), "ELSE") == 0)
	{
		cout << "# ELSE" << endl;
		readToken();								// read 'ELSE' token
		Statement();								// Generate assembly for Statement()
	}
	cout << "ENDIF" << tagNbr << ":" << endl;
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void)
{
	unsigned int tagNbr = ++TagNumber;

    if( strcmp(tokenText(), "WHILE") == 0)
    {
        cout << "WHILE" << tagNbr << ":" << endl;
        readToken();
        TYPE type = Expression();
		if( type != BOOL ) Error("Expected BOOL Expression");
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tjz ENDWHILE" << tagNbr << endl;
        if( strcmp(tokenText(), "DO") == 0)
        {
            cout << "DO" << tagNbr << ":" <<  endl;
            readToken();
            Statement();
			cout << "\tjmp WHILE" << tagNbr << endl;
			cout << "ENDWHILE" << tagNbr << ":" << endl;
        }
        else
            Error("Expected DO keyword");
    }
}

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
// Increment sign will be deduced automatically
void ForStatement(void)
{
	unsigned int tagNbr = ++TagNumber;

    if( strcmp(tokenText(), "FOR") == 0)
    {
		TYPE type1;
		TYPE type2;

        cout << "FOR" << tagNbr << ":" << endl;
        readToken();

        string var = AssignementStatement();

        type1 = getVarType( var );

        if( type1 != INT )
        {
            Error("Expected INT type not '" + getTypeStr( type1 ) +"' in FOR assignement");
        }

        if( strcmp(tokenText(), "TO") == 0 or strcmp(tokenText(), "DOWNTO") == 0)
        {

			cout << "FORCOMP" << tagNbr << ":" << endl;
            readToken();
            type2 = Expression(); // loop while var diff from SimpleExpression
			if( type2 == INT )
			{
				// Expression will be evaluated as a number
				cout << "\tpush " << var << endl;
				cout << "\tpop %rax\t\t\t# var -> rax" << endl; // var -> rax
				cout << "\tpop %rbx\t\t\t# expr -> rbx" << endl; // expr -> rbx
				cout << "\tcmpq %rax, %rbx" << endl;
				cout << "\tje ENDFOR" << tagNbr << endl;
				cout << "\tjl DECR" << tagNbr << endl;
				cout << "\tjg INCR" << tagNbr << endl;
				if( strcmp(tokenText(), "DO") == 0)
				{					
                    readToken();

					cout << "DECR" << tagNbr << ":" << endl;
					cout << "\tpush $-1" << endl;		
					cout << "\tjmp DO" << tagNbr << endl; // jump over INCR

					cout << "INCR" << tagNbr << ":" << endl;
					cout << "\tpush $1" << endl;

					cout << "DO" << tagNbr << ":" << endl;

					Statement();



					cout << "\tpush " << var << endl;
					cout << "\tpop %rbx\t\t\t# rbx -> INCR or DECR" << endl; // Incr = +1 or -1
					cout << "\tpop %rax" << endl; // var
					cout << "\taddq %rbx, %rax" << endl;
					cout << "\tpush %rax" << endl;
					cout << "\tpop " << var << endl;
					cout << "\tjmp FORCOMP" << tagNbr << endl;
					cout << "ENDFOR" << tagNbr << ":" << endl;
				}
				else
					Error("Expected DO Keyword");
			}
			else
				Error("Expected Integer Expression, not " + getTypeStr( type2 ));
        }
		else
			Error("Expected TO Keyword");
    }
	else
		Error("Expected FOR Keyword");
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
	unsigned int tagNbr = ++TagNumber;

    if( strcmp(tokenText(), "BEGIN") == 0)
	{
		cout << "BEGIN" << tagNbr << ":" << endl;
		readToken();										// read 'BEGIN'

		while( strcmp(tokenText(), "END") != 0)				// While current token is not 'END'
		{
			if( strcmp(tokenText(), "BEGIN") == 0)			// Recursive Call
				BlockStatement();
			else
				Statement();

		}
		if( strcmp(tokenText(), "END") == 0)				// Allow BEGIN END; Without statement inside
		{
			cout << "END_BEGIN" << tagNbr << ":" << endl;
			readToken();									// read 'END'
			return;
		}
	}
	else
		Error("Expected BEGIN Keyword");
}

void LParent(void)
{
	if( current != LPARENT )
		Error("Expected '(' token'");
	else
		readToken();
}

void RParent(void)
{
	if( current != RPARENT )
		Error("Expected ')' token'");
	else
		readToken();
}


void DispSubPart( void )
{
	unsigned int tagNbr = ++TagNumber;
	cout << "DISP" << tagNbr <<  ":" << endl;

	TYPE type = Expression(); // read Expression 

	if( type == INT  )
	{
		cout << "\tpop %rdx" <<endl;	
		cout << "\tmovq $FormatStrInt" << ", %rsi"  << endl;
		cout << "\tmovl    $1, %edi" << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    __printf_chk@PLT" << endl;
	}
	else if( type == FLOAT )
	{
		cout << "\tmovsd	(%rsp), %xmm0		# &stack top -> %xmm0" << endl;
		cout << "\tmovq $FormatStrFloat, %rdi" << endl;
		cout << "\tmovq	$1, %rax" << endl;
		cout << "\tcall printf" << endl;
		cout << "\taddq $8, %rsp" << endl;
		cout << "\tnop" << endl;
	}
	else if( type == BOOL )
	{
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tje DISP_FALSE" << tagNbr << endl;
		cout << "\tjne DISP_TRUE" << tagNbr << endl;

		cout << "DISP_FALSE" << tagNbr << ":" << endl;
		cout << "\tmovq $FormatStrBoolFalse, %rsi    # \"FALSE\"" << endl;
		cout << "\tmovl    $1, %edi" << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    __printf_chk@PLT" << endl;
		cout << "\tjmp DISP_END"  << tagNbr << endl;

		cout << "DISP_TRUE" << tagNbr << ":" << endl;
		cout << "\tmovq $FormatStrBoolTrue, %rsi    # \"TRUE\"" << endl;
		cout << "\tmovl    $1, %edi" << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    __printf_chk@PLT" << endl;
		cout << "DISP_END" << tagNbr << ":" << endl;
	}
	else if( type == STRING )
	{
		cout << "\tleaq	msg" << str_cpt << "(%rip), %rdi" << endl;
		cout << "\txorl	%eax, %eax" << endl;
		cout << "\tcall	printf" << endl;
	}

	else
	{
		Error("Unexpected type ( " + getTypeStr( type ) + " for 'DISP' function argument");
	}
}

// Reserved Function
TYPE CallDISP(void)
{
	int tagNbr = ++TagNumber;
	if( strcmp(tokenText(), "DISP" ) == 0)
	{
		readToken(); // read DISP
		if( current == LPARENT )
		{
			readToken();
			DispSubPart();
			while( current == COMMA )
			{
				readToken(); // read comma
				DispSubPart();
			}
			cout << "\t.data" << endl;
			cout << "DISP_ENDL" << tagNbr << ":" << endl;
			cout << "\t.string  \"\\n\"\t\t\t# the string to print." << endl;
			cout << "\t.text" << endl;
			cout << "\tleaq	DISP_ENDL" << tagNbr <<"(%rip), %rdi" << endl;
			cout << "\txorl	%eax, %eax" << endl;
			cout << "\tcall	printf" << endl;
		}
		else
			Error("Expected '(' token after reserved function 'DISP'");
	}
	else
		Error("Expected keyword 'DISP'");
	return VOID;
}

// Reserved Function
TYPE CallABS( void )
{
	if(  strcmp(tokenText(), "ABS") == 0)
		readToken();
	else
		Error("Expected 'ABS' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	cout << "ABS" << tagNbr << ":" << endl;
	if( type == INT )
	{
		cout << "\tpop %rax" << endl; // pop expr	
		cout << "\tmovq $0, %rbx" << endl;
		cout << "\tcmpq %rax, %rbx" << endl;
		cout << "\tmovq $-1, %rbx" << endl;
		cout << "\tjl END_ABS" << tagNbr << endl;
		cout << "\timulq %rbx" << endl;
		cout << "END_ABS" << tagNbr << ":" << endl;
		cout << "\tpush %rax" << endl;
		
		return INT;
	}
	else if( type == FLOAT ) // not done yet
	{
		cout << "\tfldl (%rsp)     # put expr to st( 0 )" << endl; // put expr	st[ expr ]
		cout << "\tpush $-1" << endl;
		cout << "\tfild (%rsp)" << endl;		// push and convert -1 to float st[ -1.0; expr ] 
		cout << "\taddq $8, %rsp \t\t\t# remove one value from rsp" << endl;
		cout << "\tpush $0" << endl;
		cout << "\tfild (%rsp)" << endl;		// push and convert 0 to float  st[ 0.0; -1.0; expr]
		cout << "\taddq $8, %rsp \t\t\t# remove one value from rsp" << endl;
		cout << "\tfcomip %st(2)" << endl;									//	st[ -1.0; expr ] 
		cout << "\tjc END_ABS" << tagNbr << endl;
		cout << "\tfmul %st(0), %st(1)" << endl;
		cout << "END_ABS" << tagNbr << ":" << endl;
		cout << "\tfstpl (%rsp)     # clear st" << endl;			// st[ expr ] rsp[ -1.0 ]
		cout << "\tfstpl (%rsp)     # push abs value to rsp" << endl;			// overwrite top of rsp with expr

		return FLOAT;
	}
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for ABS function");
	return UNDEFINED; // avoid warning

}

// Reserved Function 
TYPE CallSIN( void )
{
	if(  strcmp(tokenText(), "SIN") == 0)
		readToken();
	else
		Error("Expected 'SIN' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	if( type == FLOAT )
	{
		cout << "SIN" << tagNbr << ":" << endl;
		cout << "\tfldl (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfsin \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	if( type == INT )
	{
		cout << "SIN" << tagNbr << ":" << endl;
		cout << "\tfild (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfsin \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for 'SIN' function");
	return UNDEFINED; // avoid warning
}

// Reserved Function
TYPE CallCOS( void )
{
	if(  strcmp(tokenText(), "COS") == 0)
		readToken();
	else
		Error("Expected 'COS' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	if( type == FLOAT )
	{
		cout << "COS" << tagNbr << ":" << endl;
		cout << "\tfldl (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfcos \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	if( type == INT )
	{
		cout << "COS" << tagNbr << ":" << endl;
		cout << "\tfild (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfcos \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for 'COS' function");
	return UNDEFINED; // avoid warning
}

// Reserved Function
TYPE CallSQRT( void )
{
	if(  strcmp(tokenText(), "SQRT") == 0)
		readToken();
	else
		Error("Expected 'SQRT' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	if( type == FLOAT )
	{
		cout << "SQRT" << tagNbr << ":" << endl;
		cout << "\tfldl (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfsqrt \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	if( type == INT )
	{
		cout << "SQRT" << tagNbr << ":" << endl;
		cout << "\tfild (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
		cout << "\taddq $8, %rsp \t\t\t# remove top value of %rsp" << endl;
		cout << "\tfsqrt \t\t\t# st(0) -> cos( st(0))" << endl;
		cout << "\tsubq $8, %rsp    # Make room for a value" << endl;
		cout << "\tfstpl (%rsp)     # Pop %st(0) at the top of %rsp" << endl;
		return FLOAT;
	}	
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for 'COS' function");
	return UNDEFINED; // avoid warning
}

// Reserved Function
TYPE CallCHOP( void )
{	
	if(  strcmp(tokenText(), "CHOP") == 0)
		readToken();
	else
		Error("Expected 'CHOP' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	if( type == FLOAT )
	{
		cout << "CHOP" << tagNbr << ":" << endl;
		cout << "\tmovsd    (%rsp), %xmm0		# %xmm0 -> a" << endl;
		cout << "\taddq $8, %rsp" << endl;
		cout << "\tmovq $0, %rax" << endl;
		cout << "\tcvttsd2si %xmm0, %rax\t\t\t# convert float to int" << endl;		// convert float to int
		cout << "\tpush %rax" << endl;

		return INT;
	}	
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for 'CUT' function");
	return UNDEFINED; // avoid warning
}

// Reserved Function
TYPE CallROUND( void )
{	
	if(  strcmp(tokenText(), "ROUND") == 0)
		readToken();
	else
		Error("Expected 'ROUND' token ");
	LParent();					// read left parent
	int tagNbr = ++TagNumber;
	TYPE type = Expression();	// read Expression 
	if( type == FLOAT )
	{
		cout << "ROUND" << tagNbr << ":" << endl;

		cout << "\tfldl (%rsp)  \t\t\t# push top of %rsp at %st(0)" << endl;
        cout << "\tmovq $0, (%rsp) \t\t\t#clear top value of %rsp" << endl;
		cout << "\tfrndint" << endl;
        cout << "\tfstpl (%rsp)	\t\t\t# pop st(0) replacing the top value of rsp" << endl;
        // CHOP -> convert to int
		cout << "\tmovsd    (%rsp), %xmm0		# %xmm0 -> a" << endl;
		cout << "\taddq $8, %rsp" << endl;
		cout << "\tmovq $0, %rax" << endl;
		cout << "\tcvttsd2si %xmm0, %rax\t\t\t# convert float to int" << endl;		// convert float to int
		cout << "\tpush %rax" << endl;

		return INT;
	}	
	else
		Error("Unexpected Type '" + getTypeStr( type ) + "' for 'CUT' function");
	return UNDEFINED; // avoid warning
}


void SemiColon(void)
{
	if( current == SEMICOLON )
		readToken();					// read ';' token
	else
		Error("Expected ';' token");
}

TYPE ReservedCall( void ) // can be both a statement and an expr
{									
	if( current == RCALL )						// DISP is a statement
	{											// While ABS is an expr		
		string funcName = tokenText();
		TYPE type;
		if( strcmp(tokenText(), "DISP") == 0)		// Needs to be closed with a semicolon
			type = CallDISP();

		else if( strcmp(tokenText(), "ABS") == 0)		// Needs to be closed with a semicolon
			type = CallABS();

		else if( strcmp(tokenText(), "SIN") == 0)		// Needs to be closed with a semicolon
			type = CallSIN();

		else if( strcmp(tokenText(), "COS") == 0)		// Needs to be closed with a semicolon
			type = CallCOS();

        else if( strcmp(tokenText(), "SQRT") == 0)		// Needs to be closed with a semicolon
			type = CallSQRT();
		
		else if( strcmp(tokenText(), "CHOP") == 0)		// Needs to be closed with a semicolon
			type = CallCHOP();

		else if( strcmp(tokenText(), "ROUND") == 0)		// Needs to be closed with a semicolon
			type = CallROUND();

		else // should not happen unless this function is not to date compared to tokeniser.l
			Error("Unexpected Reserved Call");

		if( current == RPARENT )
			readToken();
		else
			Error("Expected ')' token after Reserved function call '" + funcName + "'");
		return type;
	}
	return UNDEFINED;
}

void Statement(void) // 
{
    if( strcmp(tokenText(), "IF") == 0)				// Contains at least one statement with a semicolon
        IfStatement();
    else if( strcmp(tokenText(), "WHILE") == 0)		// same
        WhileStatement();
    else if( strcmp(tokenText(), "FOR") == 0)		// same
        ForStatement();
    else if( strcmp(tokenText(), "BEGIN") == 0)		// same
        BlockStatement();
	else if( current == RCALL )
	{
		ReservedCall();
		SemiColon();
	}
    else if( current == ID )						// Needs to be closed with a semicolon
	{
       AssignementStatement();
	   SemiColon();
	}
	else
		Error("Expected a statement");
}

void StatementPart(void)
{
	cout << "\t.text\t\t# The following lines contain the program" << endl;
	cout << "\t.globl main\t# The main function must be visible from outside" << endl;
	cout << "main:\t\t\t# The main function body :" << endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top" << endl;
	
	bool stop = false;

	if(strcmp(tokenText(), "START") == 0)
	{
		readToken();											// read 'START' token
		if( strcmp(tokenText(), "STOP") == 0)					// allow START STOP without Statement indise
		{
			readToken();
			return;
		}

		while( !(strcmp(tokenText(), "STOP") == 0))
		{
			Statement();
		}
		if(strcmp(tokenText(), "STOP") == 0)
		{
			readToken();
		}
	}
	else
		Error("Expected 'START' token");
}

// Program := [DeclarationPart] StatementPart
void Program(void)
{
	if( strcmp( tokenText() ,"VAR") == 0)
	{
		DeclarationPart();
	}
	StatementPart();	
}

int main(void)
{	
	cout << "\t\t\t# This code was produced by the CERI Compiler" << endl;
	// Analysis and Code production
	readToken();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top" << endl;
	cout << "\tret\t\t\t# Return from main function" << endl;
	if(current!=FEOF)
    {
		Error("Unexpected characters after 'STOP' token"); // unexpected characters at the end of program
	}
}
		
			





