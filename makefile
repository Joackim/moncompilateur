all:	test
clean:
		rm *.o *.s
		rm tokensiser.cpp
tokeniser.cpp:	tokeniser.l
		flex++ -d -o tokeniser.cpp tokeniser.l
tokeniser.o:	tokeniser.cpp
		g++ -c tokeniser.cpp
compiler:	compiler.cpp tokeniser.o
		g++ -ggdb -o compiler compiler.cpp tokeniser.o
test:		compiler test.p
		./compiler <test.p 1>test.s
		gcc -ggdb -no-pie -fno-pie test.s -o test 
