# A simple compiler for a simple langage.

I call the langage recognized by this compiler **Basal** 
(for **B**rief **A**nd **S**imple **A**ll-purpose **L**angage, although,
 it is not quite All-purpose yet).

**From** : Pascal-like imperative LL(k) langage<br>
**To** : 64 bit 80x86 assembly langage (AT&T)<br>
**Made by** : Joackim Orciere

# Features

A somewhat exhaustive list of the langage's features.<br>
You can find the full grammar at the bottom of this file.

## Types
	
Declaration used for examples : 

	VAR
		BOOL  :	b;  	>>> declare a  Boolean variable named b <<< 
		INT   : i;  	>>> declare an Integer variable named i <<<	
		FLOAT : f;  	>>> declare a  Float   variable named f <<<
		
This declaration has been used in all examples, but is just show here to make things more concise.
		
### BOOL
Basic Boolean representation<br>
Can be assigned through Relatives expressions or directly with True and False.<br>
**BOOL** variables are False by default.<br>
**DISP** function will write either **FALSE** or **TRUE** instead of **0** or **1** like in C.<br>
	
**Example :**

	b := 3 < 4;  	>>> b will take the result of 3 < 4, which is True <<<
	b := False;   	>>> b will take the value False <<<

### INT
Basic 64bit Signed Integer type<br>
**INT** are initialized at **0**.<br>
	
**Example :**

	f := 3.14;
	i := f + 1; 		>>> this is incorrect, because f + 1 will be evaluated as a float <<<
	--> Compilation Error : "incompatible types, (INT, FLOAT) for assignement operator ':='"
	f := 3.14;
	i := CHOP( f + 1 ); 	>>> this is the correct way, i's value is now 4, cf CHOP function<<<

Note : you can also use ROUND to convert a **Float** to an **Int**.

### FLOAT
Basic 64bit Signed Float type<br>
**FLOAT** are initialized at 0.<br>
Int are automatically converted to **Float** if mixed with them.<br>
**Float** and **Int** can be compared.<br>
	
**Example :**

	f := 3.14 + 1;			>>> f's value is now 4.14 <<<
	f := 7;				>>> f's value is now 7.0  <<<
	b := 3.14 < 18			>>> b's value is now TRUE <<<

### VOID
This is a type that certain reserved functions returns, like **DISP** does.<br>
This is only used to prevent assignement where it would be nonsensical.

**Example :**

	f := DISP("Helloworld"); 
	--> Compilation Error : "Incompatible types, (FLOAT, VOID) for assignement operator ':='"

### STRING
This is a type that is not fully implemented yet, it is currently only used for the DISP function.<br>
Support various characters and both **'\t'** and **'\n'**.

**Example :**

	DISP("String \nTO \tDisplay ");

## Reserved Functions

Note:
  Functions can be used both as a Factor and a Statement.<br>
  When used as a Statement, a semicolon is expected to end the Statement,<br>

### DISP
Used to display any expression and strings.<br>
It can take any number of arguments, as long as they are separated by a comma **','**.<br>

**Usage :**

	CallDISP := "DISP" "(" Expr {"," expr} ")"
	--> Expr can be INT, FLOAT, BOOL or STRING
	--> returns VOID type
	 
**Example :**

	DISP("Helloworld ", 3.14159, "\n", True);

### CHOP
Used to convert from **FLOAT** to **INT**, returning the integer part of the Float.

**Usage :**

	CallCHOP := "CHOP" "(" Expr ")"
	--> Expr needs to be FLOAT
	--> returns INT type
	
**Example :**
		
	i := CHOP( 3.14 );			>>> i's value is now 3 <<<
	i := CHOP( 3.75 );			>>> i's value is now 3 <<<

### ROUND
Used to convert from **FLOAT** to **INT**, returning the nearing **Int** from the **Float** value.

**Usage :**

	CallROUND := "ROUND" "(" Expr ")"
	--> Expr needs to be FLOAT
	--> returns INT type
	
**Example :**
		
	i := ROUND( 3.14 );			>>> i's value is now 3 <<<
	i := ROUND( 3.75 );			>>> i's value is now 4 <<<
	
### ABS
Return the opposite of a negative expression.

**Usage :**

	CallABS := "ABS" "(" Expr ")"
	--> Expr can be INT, FLOAT
	--> returns Type of expr
	
**Example :**

	f := ABS( -3.14 );		>>> f's value is now 3.14 <<<
	
### SIN and COS
Returns the value of Sine/Cosine function applied to an expression.

**Usage :**

	CallSIN := "SIN" "(" Expr ")"
	CallCOS := "COS" "(" Expr ")"
	--> Expr can be INT or FLOAT
	--> returns FLOAT type
	
**Example :**

	f := SIN( -3.14159 );		>>> f's value is now ~= 0 <<<
	f := COS( 3.14 + 10 );
	
### SQRT
Returns the square root of an expression.<br>
Be careful not to call SQRT on a negative expression.<br>

**Usage :**

	CallSQRT := "SQRT" "(" Expr ")"
	--> Expr can be INT, FLOAT
	--> returns Float Type
	
**Example :**

	f := SQRT( 9.0 );		>>> f's value is now 3.0 <<<
	
## Operators

**Usage :**

	AdditiveOperator := "+" | "-" | "OR"
	MultiplicativeOperator := "*" | "/" | "%" | "AND"
	RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
	
List of operators behaviors :

	INT   (+|-) INT   --> INT
	INT   (+|-) FLOAT --> FLOAT
	FLOAT (+|-) INT   --> FLOAT
	FLOAT (+|-) FLOAT --> FLOAT
	
	BOOL  OR  BOOL  --> BOOL
	BOOL  AND BOOL  --> BOOL
	(INT | FLOAT) RelationnalOperator (INT | FLOAT) -> BOOL
	BOOL !=|== BOOL -> BOOL
	
	(INT | FLOAT) (*|/) (INT | FLOAT) -> FLOAT
	INT % INT -> INT
	
Note : <br>
  **INT / INT** will return a **Float** whereas **INT * INT** will return an **Int**<br>
  All Arithmetics Expressions containing a **Float**, will be evaluated as **Float** by default.<br>


## Program Layout

General layout of a basal program.

**Usage :**

	Programm := [DeclarationPart] StatementPart
	
Notes :<br>
  DeclarationPart starts with the keyword **"VAR"**<br>
  StatementPart starts with the keyword **"START"** and ends with **"STOP"**<br>
        
	DeclarationPart := "VAR" { DeclarationSubPart }
	DeclarationSubPart := Type ":" ID { "," ID } ";"
	StatementPart := "START" { Statement } "STOP"
	
**Example :**
	
	>>> 
	    this code will compute and display the sum of two integer
	<<<
	VAR
		INT : a, b;        >>> This is a DeclarationSubPart <<<
	START
		a :=  5;           >>> This is a Statement <<<
		b := -3;
		DISP( a + b );
	STOP
	
## Structures

The overall structure of a program.

### Comments

Used to discard a specific portion of the code.
Comments are multilines. However they cannot be nested.

**Usage :**

	">>>" Any characters until closure will be discarded "<<<"	
	
### Statement

Only AssignementStatement and ReservedFunctions (when as a Statement) end with a semicolon **';'**.<br>

**Example :**

	f := COS( -3.14 ) + 1.0;    >>> Cos is used as a factor and is not terminated by ';' but the Assignement is <<<
	DISP( f );                  >>> Disp is used as a Statement and is terminated by ';'<<<
	COS( -3.14 );               >>> Not prohibited yet, but definitly useless <<<
	
**Usage :**

	Statement := IfStatement | WhileStatement | ForStatement | 
	             BlockStatement | ReservedFunction ";" | AssignementStatement ";"

	AssignementStatement := Identifier ":=" Expr
	
### IF Statement

Notes :
  only the first block having a condition evaluated as **True** will be executed.<br>
  If none are **True** the else block will be executed.<br>

**Usage :**

	IfStatement := "IF" Expr "THEN" Statement { "ELSIF" Expr "THEN" Statement } [ "ELSE" Statement ]
	--> Expr needs to be BOOL 
	
**Example :**

	i := 5;
	IF i > 10 THEN                      >>> False, will not be executed <<<
		DISP("i is big");
	ELSIF i < 6 THEN                    >>> True, will be executed <<<
		DISP("i is small");
	ELSIF i == 5 THEN                   >>> True, but won't be executed <<<     
		DISP("i is 5");
	ELSE                                >>> Always True, but won't be executed <<<
		DISP("size doesn't matter");

### FOR Statement

Used to create loops with a know number of iterations.<br>
This For as an implicit, deduced increment.<br>
It will automatically increment or decrement accordingly always with a step of **1**.<br>

**Usage :**
	
	ForStatement := "FOR" assignementStatement "TO" expr2 "DO" Statement
	--> both expr1 and expr2 need to be INT 
	
**Example :**
	
	FOR i:=3 TO -2 DO DISP(i);       >>> i will take the values : 3, 2, 1, 0, -1 <<<

Note:
  Be careful not to modify the assigned variable, inside the For block, or it could break.<br>

### BLOCK Statement

Used to put severals statements inside a structure such as a IF or a WHILE.

**Usage :**
	
	BlockStatement := "BEGIN" { Statement } "END"
	--> Expr needs to be BOOL

**Example :**
See While example below

### WHILE Statement

Used to create loops without a known number of iterations<br>

**Usage :**
	
	WhileStatement := "WHILE" Expr "DO" Statement
	--> Expr needs to be BOOL
	
**Example :**

	i := 0;
	b := True;
	WHILE b DO 
	BEGIN
		IF i == 5 THEN b:=False;
		i := i + 1;
	END
	
	
## Grammar

	Program := [DeclarationPart] StatementPart
	DeclarationPart := "VAR" { DeclarationSubPart }
	DeclarationSubPart := Type ":" ID { "," ID } ";"
	Type = BOOL|INT|FLOAT
	
	StatementPart := "START" { Statement } "STOP"
	Statement := IfStatement 
	           | WhileStatement 
	           | ForStatement 
	           | BlockStatement 
	           | ReservedFunction ";" 
	           | AssignementStatement ";"
	
	ReservedFunction := CallDISP | CallABS | CallSIN | CallCOS | CallSQRT | CallCHOP | CallROUND
	AssignementStatement := Identifier ":=" Expression

	IfStatement := "IF" Expr "THEN" Statement { "ELSIF" Expr "THEN" Statement } [ "ELSE" Statement ]
	WhileStatement := "WHILE" Expr "DO" Statement
	ForStatement := "FOR" assignementStatement "TO" expr2 "DO" Statement
	BlockStatement := "BEGIN" { Statement } "END"
	
	DispCall := "DISP" "(" Expr {"," expr} ")"
	
	Expr := SimpleExpression [RelationalOperator SimpleExpression]
	SimpleExpression := Term {AdditiveOperator Term}
	Term := Factor {MultiplicativeOperator Factor}
	Factor := Number | Identifier | "(" Expression ")"| ReservedFunction | StringConst
	
	StringConst := \"[A-Za-z0-9 \\\(\)\<\>\=\!\%\&\|\}\-\:\;\.]*\"
	Number := (\-)?{digit}+|True|False|(\-)?{digit}+\.{digit}+
	Identifier := Letter { (Letter|Digit) }

	AdditiveOperator := "+" | "-" | "OR"
	MultiplicativeOperator := "*" | "/" | "%" | "AND"
	RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
	Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
	Letter := "a"|...|"z"
	
## Dependencies

* **Flex**
* **gcc**
* **make**

## Build

	make compiler

## Author

* **Joackim Orciere**

## License

This program is free software: you can redistribute it and/or modify it under the terms<br>
of the GNU General Public License as published by the Free Software Foundation, <br>
either version 3 of the License, or any later version, see <https://www.gnu.org/licenses/><br>



